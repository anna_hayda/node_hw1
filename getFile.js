const express = require('express');
const read = express.Router();
const fs = require('fs');
const path = require('path');

const validFile = '^.*\\.(log|txt|json|yaml|xml|js)$'

read.get('/api/files/:filename', (req, res) => {
    const {filename} = req.params;
    const pathToFile = path.join(__dirname, 'files', filename);

    if (!filename.match(validFile)) {
        return res.status(400).json({message: "Please specify correct file type"});
    }

    if (!fs.existsSync(pathToFile)) {
        return res.status(400).json({message: `No file with '${filename}' filename found`});
    }

    fs.readFile(pathToFile, 'utf-8', (err, content) => {
        if (!err) {
            res.status(200)
                .json({
                    message: "Success",
                    filename: filename,
                    content: content,
                    extension: path.extname(filename).slice(1),
                    uploadedDate: fs.statSync(pathToFile).birthtime
                });
        } else {
            res.status(500).json({message: 'Server error'});
            throw err;
        }
    });
});

module.exports = read;
